# Create custom forms here
from django import forms

class ContactForm(forms.Form):
    # label = '' to suppress automatic labelling above fields
    email = forms.EmailField(widget = forms.TextInput(attrs={'placeholder': 'E-Mail'}), label = '', required = True)
    subject = forms.CharField(widget = forms.TextInput(attrs={'placeholder': 'Subject'}), label = '', required = True, max_length=280)
    message = forms.CharField(widget = forms.Textarea(attrs={'rows':'7', 'placeholder': 'Your Message'}), label = '', required = True)

