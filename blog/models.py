from django.db import models
from django.urls import reverse
from django.db.models.signals import pre_save
from django.template.defaultfilters import slugify # For generating custom urls for each article
from django.contrib.auth.models import User
from .utils import unique_slug_generator # Unique slug generator we created

# Create your models here.
class Post(models.Model):
    '''
        title
        category
        author - Foreign key to User model
        date_posted
        content
        slug (future)
    '''
    title = models.CharField(max_length = 255)
    author = models.ForeignKey(User, on_delete = models.CASCADE)
    category = models.CharField(max_length = 255)
    date_posted = models.DateTimeField(auto_now_add = True)
    content = models.TextField()
    slug = models.SlugField(allow_unicode = True, max_length = 255, blank = True)

    # Simply returns the absolute url to the current instance
    # Used to redirect user to post-detail after they create a post
    def get_absolute_url(self):
        return reverse("post-detail", kwargs={"slug": self.slug})
    

    def __str__(self):
        return self.title

def slug_save(sender, instance, *args, **kwargs):
    '''
        If slug does not exist, generate unique slug
        If existing slug not the same as title, 
    '''
    if not instance.slug:
        instance.slug = unique_slug_generator(instance, instance.title, instance.slug, new = True)
    elif instance.slug != slugify(instance.title):
        instance.slug = unique_slug_generator(instance, instance.title, instance.slug, new = False)

pre_save.connect(slug_save, sender=Post)

    # def get_absolute_url(self):
    #     return reverse("post-detail", kwargs={"slug": self.slug})

    # def save(self, *args, **kwargs):
    #     if not self.slug:
    #         # i.e if slug not provided by user, create one based on title.
    #         self.slug = slugify(self.title)
    #     super(Post, self).save(*args, **kwargs)
