# TODO

Follow Corey's format

* ~~Create a new user 'Red'~~ - Done
* ~~Add some posts with user Red~~ - Done
* ~~Dev Server needs to be restarted each time for posts to update.~~ - Fixed
* ~~Create PostDeleteView~~ Done
* Include CKEditor/Markdown Editor
* Fix 403 not redirecting when logged in user attempts to edit/delete other user's posts.
* Create a general purpose error page.
* Setup mail server and configure

* ~~Fix Archives:~~ (Done)
    * ~~Add a page for archives, listing all the months posted (`ListView`).~~
    * ~~Add a page for each month, listing all posts in that month (`ListView`).~~
    * ~~On sidebar add archive links -> respective month page and 'More' button.~~

    * ~~`post_dates["Post4"].strftime("%b %Y")`~~

* ~~Create Categories Page:~~ (Done)
    * ~~If not exists then 404~~
    * ~~ListView~~
    * ~~Same as author_posts and archive~~

## Questions:
>Whats the difference b/w STATIC_URL and STATIC_ROOT?

>Whats the difference b/w `{% load static %}` and `{% load staticfiles %}`?