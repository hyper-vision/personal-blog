from django.contrib import admin
from django.urls import path
from .views import (PostDetailView, 
                    PostCreateView,
                    PostUpdateView,
                    PostDeleteView,
                    PostListView,
                    UserPostListView,
                    PostDateListView,
                    PostCategoryListView,
                    DashboardListView) # Import class view to assing to URL
import blog.views

urlpatterns = [
    # path('', blog.views.index, name = 'blog-home'),
    path('', PostListView.as_view(), name = 'blog-home'),
    path('about/', blog.views.about, name = 'blog-about'),
    path('posts/', blog.views.posts, name = 'blog-posts'),
    path('post/new/', PostCreateView.as_view(), name = 'post-create'),
    path('dashboard/', DashboardListView.as_view(), name = 'dashboard'),
    path('contact/', blog.views.contact, name = 'contact'),
    path('success/', blog.views.success, name = 'success'), # Success page for contact form
    # Dynamic URL changes based on post
    path('post/<str:slug>/', PostDetailView.as_view(), name = 'post-detail'),
    path('post/<str:slug>/edit/', PostUpdateView.as_view(), name = 'post-update'),
    path('post/<str:slug>/delete/', PostDeleteView.as_view(), name = 'post-delete'),
    path('user/<str:username>', UserPostListView.as_view(), name = 'user-posts'),
    path('posts/<str:date>', PostDateListView.as_view(), name = 'date-posts'),
    path('categories/<str:category>', PostCategoryListView.as_view(), name = 'category_posts')
]
