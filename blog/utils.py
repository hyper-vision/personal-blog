from django.utils.text import slugify

def unique_slug_generator(model_instance, title, slug_field, new):
    slug = slugify(title)
    model_class = model_instance.__class__ # Returns class name of current model instance (makes it possible to use this function on any model)

    if new:
        # Checks the following: Post.objects.filter(slug=slug).exists(), returns True if exists
        while model_class._default_manager.filter(slug=slug).exists():
            '''
            The while loop checks to see if the current slug exits in the db.
            If True then we append the latest_pk + 1 to the end of the slug.
            If False we skip the loop and the slugified title becomes the slug we return.
            '''

            object_pk = model_class._default_manager.latest('pk') # Returns the entry with the latest pk
            object_pk = object_pk.pk + 1 # Gets the pk of the latest entry

            slug = f"{slug}-{object_pk}"
    else:
        # IF the instance already exists and we are just editing the title
        # then generate a new slug with the same 
        slug = f"{slug}-{model_instance.pk}"
    
    return slug