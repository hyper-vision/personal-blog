from django.shortcuts import render, reverse, get_object_or_404, redirect
from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User 
from django.core.mail import send_mail, BadHeaderError # For contact form
from .models import Post
from .forms import ContactForm # Import our custom forms
from time import strptime
from django.views.generic import (ListView, 
                                  DetailView, 
                                  CreateView, 
                                  UpdateView, 
                                  DeleteView) # For individual posts

# Create your views here.

def about(request):
    return render(request, "blog/about.html")

def posts(request):
    context = {'posts': Post.objects.all().order_by('-date_posted')}
    return render(request, "blog/posts.html", context=context)

def success(request):
    return render(request, 'blog/success.html')

def contact(request):
    # TODO: Setup Mail server and configure to send
    if request.method == "GET":
        form = ContactForm()
    elif request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data["email"]
            subject = form.cleaned_data["subject"]
            message = form.cleaned_data["message"]

            try:
                send_mail(subject, message, email, ['rufusabu94@gmail.com'])
            except BadHeaderError:
                return HttpResponse("Invalid header found.") # TODO: return a valid error page
            return redirect('successs')
    # When user fills out contact form and submits it
    return render(request, "blog/contact.html", {'form':form})

class DashboardListView(LoginRequiredMixin, ListView):
    model = Post
    template_name = 'blog/dashboard.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']

class PostListView(ListView):
    model = Post
    template_name = 'blog/index.html'
    context_object_name = 'posts'
    ordering = ['-date_posted']
    paginate_by = 5
    # sidebar_posts = Post.objects.all().order_by('-date_posted')[:3]

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs) # Get the context from superclass
        context['sidebar_posts'] = Post.objects.all().order_by('-date_posted')[:3] # Add our data to the context
        context['sidebar_archive'] = set([post.date_posted.strftime("%b %Y") for post in Post.objects.all().order_by("-date_posted")])
        context['sidebar_categories'] = set([post.category for post in Post.objects.all()])
        return context

class UserPostListView(ListView):
    model = Post
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'
    paginate_by = 4

    # Get query set of current user's posts
    # Overriding built in get_queryset which returns the queryset of entries in the model
    def get_queryset(self):
        # get_object_or_404 returns the object if exists, else 404
        user = get_object_or_404(User, username = self.kwargs.get('username')) #kwargs = dynamic value passed via url
        return Post.objects.filter(author = user).order_by('-date_posted')


class PostDateListView(ListView):
    model = Post
    template_name = 'blog/posts_dateview.html'
    context_object_name = 'posts'
    paginate_by = 4

    def get_queryset(self):
        date_string = self.kwargs.get('date')
        # [month, year]
        date_values = date_string.split()
        # strptime().tm_mon converts month name to number
        return Post.objects.filter(date_posted__month = strptime(date_values[0], '%b').tm_mon, date_posted__year = date_values[1]).order_by('-date_posted')


class PostCategoryListView(ListView):
    model = Post
    template_name = 'blog/posts_categoryview.html'
    context_object_name = 'posts'
    paginate_by = 4

    def get_queryset(self):
        category_name = self.kwargs.get('category')
        return Post.objects.filter(category = category_name).order_by('-date_posted')

# Using class based views for our individual posts view
# Class views look for templates of the naming style: <app name>/<model name>_<view type>.html
# So here it will look for: blog/post_detail.html
class PostDetailView(DetailView):
    model = Post
    context_object_name = 'post' # Name to assign to model object being passed
    # By default it is 'object'

class PostCreateView(LoginRequiredMixin, CreateView):
    # Redirect to login page if not logged in
    login_url = "/login/"
    model = Post
    # Specify which fields we would like to include
    fields = ['title', 'category', 'content']
    # By default looks for template named <model>_form.html (post_form.html)

    # Override the form_valid method in CreateView to validate our user before posting.
    def form_valid(self, form):
        # Tells webapp to set the author of the post = current logged in author.
        form.instance.author = self.request.user
        return super().form_valid(form)

class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    # Same as PostCreateView, even uses the same template
    # UserPassesTestMixin makes sure that only the author of a post can edit it.

    login_url = "/login/"
    model = Post
    # Specify which fields we would like to include
    fields = ['title', 'category', 'content']
    # By default looks for template named <model>_form.html (post_form.html)

    # Override the form_valid method in CreateView to validate our user before posting.
    def form_valid(self, form):
        # Tells webapp to set the author of the post = current logged in author.
        form.instance.author = self.request.user
        return super().form_valid(form)
    
    # test_func allows us to write test conditions before allowing certain actions.
    def test_func(self):
        post = self.get_object() # Gets the current post
        if self.request.user == post.author:
            return True
        return False

class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Post
    context_object_name = 'post'
    login_url = "/login/"

    # URL to redirect to upon successful deletion
    success_url = "/"

    # Expects default template: <modelname>_confirm_delete.html
    def test_func(self):
        post = self.get_object() # Gets the current post
        if self.request.user == post.author:
            return True
        return False
